#include <iostream>
#include <chrono>
#include <set>
#include <algorithm>
#include <vector>

#include "glad/glad.h"
#include "GLFW/glfw3.h"
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/type_ptr.hpp"

#include "Window.h"
#include "Shader.h"
#include "Snake.h"

extern const int WIDTH = 300;
static const int HEIGHT = 300;
static const int TILE_SIZE = 20;
static std::set<std::pair<int,int>> grid;

struct Event_Manager 
{
    static Snake* player;
    static void init(Snake* s) { player = s; }
    static void move_snake(int x, int y) { player->move(x, y); }
};
Snake* Event_Manager::player;

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if (action == GLFW_PRESS)
    {
        switch (key)
        {
            case GLFW_KEY_UP:
            {
                Event_Manager::move_snake(0, 1);
                break;
            }
            case GLFW_KEY_DOWN:
            {
                Event_Manager::move_snake(0, -1);
                break;
            }
            case GLFW_KEY_LEFT:
            {
                Event_Manager::move_snake(-1, 0);
                break;
            }
            case GLFW_KEY_RIGHT:
            {
                Event_Manager::move_snake(1, 0);
                break;
            }
        }
    }
}

void place_food(std::set<std::pair<int,int>> *snake_body, Square* food)
{
    /* How to place food so that food location is not on the snake body?
     * - We use set difference between two sets (the grid (precalculated), and the snakes body)
     * - We then pick a random number in the range [0, size of resulting set in set difference)
     * - That number is the index at which we will place our food
    */

    // Set difference
    std::vector<std::pair<int,int>> open_tiles(grid.size());
    std::vector<std::pair<int,int>>::iterator it;
    it = std::set_difference(grid.begin(), grid.end(), snake_body->begin(), snake_body->end(), open_tiles.begin());
    open_tiles.resize(it-open_tiles.begin());

    // Choose location
    int rand_index = rand() % open_tiles.size();
    std::pair<int,int> location = open_tiles[rand_index];
    food->set_location(location.first, location.second);
}

void init_food(std::set<std::pair<int,int>> *snake_body, Square* food)
{
    const int half = TILE_SIZE / 2;
    const int sections = WIDTH / TILE_SIZE;
    for (int i = 0; i < sections; i++)
    {
        for (int j = 0; j < sections; j++)
        {
            grid.insert(std::make_pair(i * TILE_SIZE + half, j * TILE_SIZE + half));
        }
    }
    place_food(snake_body, food);
}

void check_food(Square* food, Snake* snake)
{
    std::pair<int,int> snake_location = snake->get_head_location();
    if (glm::distance(glm::vec2(food->x, food->y), glm::vec2(snake_location.first, snake_location.second)) < 2.0f)
    {
        // update snake
        snake->add_tile();

        std::set<std::pair<int,int>> snake_body = snake->get_body();
        place_food(&snake_body, food);
    }
}

int main()
{
    /* Window Initialization
     * window cannot be resized
    */
    Window window = Window(WIDTH, HEIGHT, "OpenGL Window");
    window.set_window_size(WIDTH, HEIGHT, WIDTH, HEIGHT);
    window.set_key_callback(key_callback);

    /* Shader Initialization
    */
    Shader shader = Shader("src/shader/shader.vert", "src/shader/shader.frag");
    unsigned int uniform_model_location = shader.get_uniform_location("u_model");
    unsigned int uniform_projection_location = shader.get_uniform_location("u_projection");
    unsigned int uniform_color_location = shader.get_uniform_location("u_color");
    glm::mat4 projection = glm::ortho(0.0f, (float)WIDTH, 0.0f, (float)HEIGHT, -1.0f, 1.0f);

    /* Square Initialization
     * we must init so the vertices (a static member) can be set
     * vertices are dependent on the tile size
    */
    Square::init(TILE_SIZE);

    /* Snake initialization
    */
    Snake snake = Snake(WIDTH / 2, HEIGHT / 2, WIDTH, HEIGHT, TILE_SIZE);

    /* Food initialization
    */
    srand(time(0));
    Square food_square = Square(0,0);
    std::set<std::pair<int,int>> snake_body = snake.get_body();
    init_food(&snake_body, &food_square);

    /* Event Manager initialization
    */
    Event_Manager::init(&snake);

    /* FPS initialization
    */
    std::chrono::steady_clock::time_point t1 = std::chrono::steady_clock::now();
    std::chrono::steady_clock::time_point t2;
    std::chrono::duration<double> tick_time;
    double time_span = 0;

    while (!window.should_close())
    {
        t2 = std::chrono::steady_clock::now();
        tick_time = std::chrono::duration_cast<std::chrono::duration<double>>(t2-t1);
        t1 = t2;
        time_span += tick_time.count();

        if (time_span > 0.1)
        {
            glfwPollEvents();
            check_food(&food_square, &snake);
            snake.update();    
            
            glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
            glClear(GL_COLOR_BUFFER_BIT);

            shader.use();

                glUniformMatrix4fv(uniform_projection_location, 1, GL_FALSE, glm::value_ptr(projection));
                food_square.render(uniform_model_location, uniform_color_location, 1.0f, 0.0f, 0.0f);
                snake.render(uniform_model_location, uniform_color_location);

            glUseProgram(0);

            time_span = 0;
            window.swap_buffers();
        }
    }
    glfwTerminate();
    return 0;
}