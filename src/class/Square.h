#include <iostream>

#include <glad/glad.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

class Square
{
    static float vertices[];
    static unsigned int VAO, VBO;

    public:
        static void init(const int tile_size);

        int x, y;
        Square(const int x, const int y);
        void set_location(const int x, const int y);
        void render(unsigned int uniform_model_location, unsigned int uniform_color_location, const GLfloat r, const GLfloat g, const GLfloat b);
};