#ifndef SHADER_H
#define SHADER_H

#include <glad/glad.h> // include glad to get all required openGL headers
#include <glm/glm.hpp>

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>

class Shader
{

    public:
        unsigned int ID;

        Shader(const char* vertex_path, const char* fragment_path);
        void use();

        // Uniform ultiliy functions
        unsigned int get_uniform_location(const std::string &name) const;

        void set_uniform_mat4(const std::string &name, glm::mat4 matrix) const;
        void set_bool(const std::string &name, bool value) const;
        void set_int(const std::string &name, int value) const;   
        void set_float(const std::string &name, float value) const;
};
#endif