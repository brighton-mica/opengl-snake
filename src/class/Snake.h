#include <iostream>
#include <deque>
#include <memory>
#include <utility>
#include <set>

#include "Square.h"


class Snake
{
    int starting_x, starting_y;
    int head_x, head_y;
    int screen_width, screen_height;
    int tile_size;
    int x_speed, y_speed;
    std::deque<std::unique_ptr<Square>> tail;

    public:
        Snake(const int x, const int y, const int width, const int height, const int tile_size);
        void move(int x, int y);
        void update();
        void render(unsigned int uniform_model_location, unsigned int uniform_color_location);
        std::pair<int,int> get_head_location();
        std::set<std::pair<int,int>> get_body();
        void add_tile();
};