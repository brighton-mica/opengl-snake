#include "Snake.h"

Snake::Snake(const int x, const int y, const int width, const int height, const int tile_size)
{
    starting_x = x;
    starting_y = y;
    head_x = x;
    head_y = y;
    screen_width = width;
    screen_height = height;
    this->tile_size = tile_size;
    x_speed = 1;
    y_speed = 0;
    tail.push_back(std::make_unique<Square>(x, y));
}

void Snake::move(int x, int y)
{
    if ((x != x_speed && y != y_speed) || tail.size() == 1)
    {
        x_speed = x;
        y_speed = y;
    }
}

void Snake::update()
{
    head_x = (screen_width + tail[0]->x + x_speed * tile_size) % screen_width;
    head_y = (screen_height + tail[0]->y + y_speed * tile_size) % screen_height;

    for (const auto &snake : tail)
    {
        if (head_x == snake->x && head_y == snake->y)
        {
            std::cout << "You had a score of " << tail.size() <<std::endl;
            exit(EXIT_SUCCESS);
        }
    }

    tail.push_front(std::make_unique<Square>(head_x, head_y));
    tail.pop_back();
}

void Snake::render(unsigned int uniform_model_location, unsigned int uniform_color_location)
{
    for (const std::unique_ptr<Square> &square : tail)
    {
        square->render(uniform_model_location, uniform_color_location, 0.0f, 1.0f, 0.0f);
    }
}

std::pair<int,int> Snake::get_head_location() { return std::make_pair(head_x, head_y); }

std::set<std::pair<int,int>> Snake::get_body()
{
    std::set<std::pair<int,int>> body;
    for (const std::unique_ptr<Square> &square : tail)
    {
        body.insert(std::make_pair(square->x, square->y));
    }
    return body;
}

void Snake::add_tile()
{
    tail.push_back(std::make_unique<Square>(tail.back()->x, tail.back()->y));
}