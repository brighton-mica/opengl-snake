#include "Square.h"

unsigned int Square::VAO = -1;
unsigned int Square::VBO = -1;
float Square::vertices[18];

void Square::init(const int size)
{
    float half = size / 2.0f;
    vertices[0] = -half; vertices[1] = -half; vertices[2] = 0.0f;
    vertices[3] =  half; vertices[4] = -half; vertices[5] = 0.0f;
    vertices[6] =  half; vertices[7] =  half; vertices[8] = 0.0f;

    vertices[9]  = -half; vertices[10] = -half; vertices[11] = 0.0f;
    vertices[12] =  half; vertices[13] =  half; vertices[14] = 0.0f;
    vertices[15] = -half; vertices[16] =  half; vertices[17] = 0.0f;

    glGenVertexArrays(1, &VAO);
    glBindVertexArray(VAO);

    glGenBuffers(1, &VBO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
}

Square::Square(const int x, const int y)
{
    this->x = x;
    this->y = y;
}

void Square::render(unsigned int uniform_model_location, unsigned int uniform_color_location, const GLfloat r, const GLfloat g, const GLfloat b)
{
    glm::mat4 model = glm::mat4(1.0f);
    model = glm::translate(model, glm::vec3((float)x, y, 0.0f));
    glUniformMatrix4fv(uniform_model_location, 1, GL_FALSE, glm::value_ptr(model));
    glUniform3f(uniform_color_location, r, g, b);

    glBindVertexArray(VAO);
    glDrawArrays(GL_TRIANGLES, 0, 6);
    glBindVertexArray(0);
}

void Square::set_location(const int x, const int y) { this->x = x; this->y = y; }