INCLUDES = -I./include -I./src/class
LIBS = -lglfw3 -lGL -lX11 -lpthread -lXrandr -lXi -ldl
OBJS = main.o glad.o Window.o Shader.o Square.o Snake.o
CFLAGS = -O3

all: a.out

a.out: $(OBJS)
	g++ -o a.out $(OBJS) $(LIBS) $(CFLAGS)

main.o: ./src/main.cpp
	g++ -c ./src/main.cpp $(CFLAGS) $(INCLUDES)

glad.o: ./src/glad.c
	g++ -c ./src/glad.c $(CFLAGS) $(INCLUDES)

Window.o: ./src/class/Window.cpp
	g++ -c ./src/class/Window.cpp $(CFLAGS) $(INCLUDES)

Shader.o: ./src/class/Shader.cpp
	g++ -c ./src/class/Shader.cpp $(CFLAGS) $(INCLUDES)

Square.o: ./src/class/Square.cpp
	g++ -c ./src/class/Square.cpp $(CFLAGS) $(INCLUDES)

Snake.o: ./src/class/Snake.cpp
	g++ -c ./src/class/Snake.cpp $(CFLAGS) $(INCLUDES)

clean:
	rm $(OBJS)